import java.util.*

fun main(args :Array<String>){
    // Printing with index
    var list = listOf<Int>(1,2,3,4)
    for ( i in list){
        println(i)
    }
    // Displaying with element with their respective index

    for ( (index,element) in list.withIndex()) {
        println("$index : $element")
    }

    //Map

    var details = TreeMap<String,Int>()
    details["saichaitany"] =15
    details["achanta sai"]  =15
    details["himaja"]   =1
    for ((name,dob) in details){
        println("$name : $dob")
    }

}