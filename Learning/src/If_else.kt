fun main(args : Array<String>){
    var num1 : Int =15
    var num2 : Int =16
    var result : Int =0

    result = if(num1 > num2)
        num1
    else
        num2
    println("Result using general if-else condition: $result")

    // Ternary Condition

    result = if(num1 > num2) num1 else num2

    println("Result using Ternary operation: $result")
}