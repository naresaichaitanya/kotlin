fun main (args : Array<String>){
/*
 *  First type of declaring the variable
 *  */
    var myString = "My first variable declaration in kotlin"

    var myNumber = 15

    var boolean = false
/*
 *  Second type of declaring the variable
 *  */

    var myValue : String

    myValue ="Second type of assigning value with desired datatype"

    println(myString)

    println(myNumber)

    println(boolean)

    println(myValue)

/*
 *  Declaring the variable as constant just like final in java
 */

    val myAnotherString = "My constant String"

}