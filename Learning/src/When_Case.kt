fun main(args : Array<String>){

    var num : Int =10
/*
    When case in kolin is just as Switch case in java
*/
    var str: String =when(num){
        10 -> "ten"
        20 -> "twenty"
        else -> "not matched"
    }
    println("Result: $str")
    /*
        We can combine values in when
    */
    var myValue : Int =0
    when(myValue){

        0,1 -> println("my value is 0 or 1")
        else -> println("otherwise")
    }

    /*
        We can also make range in when case
    */

    when(myValue){
        in 0..90 -> print("myvalue range is in between 0 -90")
        in 91..200 -> print("myvalue range is in between 91-200")
        else -> print("none")
    }
}