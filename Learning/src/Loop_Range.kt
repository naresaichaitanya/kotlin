fun main(args: Array<String>){
//    // To print 1 to 20 values
    var num = 1 .. 20
    println("Ascending Order with step-2:")
    for(x in num step 2){
        println(x)
    }
    // To print 20 to 1
     var num1= 20 downTo 1
    println("Descending Order using downTo method:")
    for(y in num1){
        println(y)
    }
    // (or)
    println("Descending Order using reversed() method:")
    for (y in num.reversed()){
        println(y)
    }

 //    To print 1 to <20..Here until is used
    var num2 = 1 until 20
    println("Displaying values upto 19")
    for (z in num2){
        println(z)
    }

    // To print characters from 'A' to 'z'
    var ch='A' .. 'z'
    for (a in ch){
        println(a)
    }

}